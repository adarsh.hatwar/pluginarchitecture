const architect = require("architect");
const config = architect.loadConfig(__dirname + "/config.js");
const app = architect.createApp(config);
app.on('service', (name, service) => {
    console.log('******************************');
    console.log('Registering service ===>', name);
    console.log('******************************\n');
});
