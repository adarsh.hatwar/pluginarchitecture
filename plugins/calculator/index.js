const { argv } = require('yargs');

module.exports = (options, imports, register) => {


    const pluginOfChoice = imports[argv.pluginName];

    console.log('Env variables available', pluginOfChoice.getEnvVariables());

    console.log(pluginOfChoice.performOperation(1 , 2));

    register(null, {});

}