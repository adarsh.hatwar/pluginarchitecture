module.exports = (options, imports, register) => {
    const api = {};
    
    api.performOperation = (op1, op2) => {
        return op1 - op2;
    }

    register(null, { subtraction: api});
}