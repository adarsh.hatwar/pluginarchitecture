const chalk = require('chalk');

module.exports = (options, imports, register) => {
    const api = {};

    api.performOperation = (op1, op2) => {
        console.log(chalk.blue('Performing addition...'));
        return op1 + op2;
    }

    api.getEnvVariables = () => {
        return options.envVariables;
   }

    register(null, { addition: api});
}