Plugin architecture
===

Sample app to showcase plugin architecture.

Doc available [here](https://docs.google.com/document/d/1sH5lSUcy35vuSlgVfsx8EgNGePy2g6_veEoxfSzOQH0/edit#heading=h.wsxk822i9ysb)

To run:

From the root directory run:

```bash
$ node index --pluginName=<pluginName>
```

Example:
```bash
$ node index --pluginName=addition
```